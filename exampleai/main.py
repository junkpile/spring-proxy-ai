#!/usr/bin/env python3

import socketserver
import random
from math import pow, sqrt

import commands
from events import EventTopic, load_event

def distance_to(pos1):
    def distance(pos2):
        return sqrt(pow(pos1['x'] - pos2['x'], 2) + pow(pos1['z'] - pos2['z'], 2))
    return distance


class MyTCPHandler(socketserver.StreamRequestHandler):

    def remove_idle_points(self, frame, points):
        timed_out = [point for point in points if point[0] < frame]

        for (timeout, (x, y, z)) in timed_out:
            commands.remove_point(self.request, x, y, z)

        return [point for point in points if point[0] >= frame]

    def add_point(self, frame, points, x, y, z, label, timeout):
        points.append((frame + timeout, (x, y, z)))
        commands.add_point(self.request,
                           x,
                           y,
                           z,
                           label)

    def handle(self):
        commanderId = None
        energy = 0.0
        metal = 0.0
        build_queue = []
        builders = set()
        army = set()
        army_units = ["armpw", "armrock", "armham", "armjeth", "armwar"]
        lab_build = army_units + ["armck"]
        stuff = ["armrad", "armllt", "armllt", "armlab"]
        spots = None
        home = None
        frame = 0
        points = []
        while True:
            jdata = self.rfile.readline()
            jd = load_event(jdata)

            if jd.get('topic') == EventTopic.EVENT_UPDATE:
                metal = (jd['meta']['resources']['Metal']['income']
                         - jd['meta']['resources']['Metal']['usage'])
                energy = (jd['meta']['resources']['Energy']['income']
                          - jd['meta']['resources']['Energy']['usage'])
                frame = jd['data']['frame']
                points = self.remove_idle_points(frame, points)
                continue

            print(" got: {}".format(jd))

            if jd.get('topic') == EventTopic.EVENT_RESOURCE_MAP:
                spots = list(jd['data']['resources']['Metal'])
                build_queue = list(jd['data']['resources']['Metal'])

            if jd.get('topic') == EventTopic.EVENT_UNIT_CREATED:
                builder = jd['data']['builder']
                unitId = jd['data']['unit']
                # Detect when the commander spawns
                if builder == -1:
                    commanderId = unitId
                    builders.add(commanderId)
                    home = jd['meta']['atPos']
                    commands.add_line(self.request, home['x'], home['y'], home['z'], 0, 0, 0)
                    points.append((frame + 1000, (home['x'], home['y'], home['z'])))

            if (jd.get('topic') ==  EventTopic.EVENT_UNIT_DAMAGED
                and jd['data']['unit'] == commanderId):

                    commands.move_to(self.request,
                                     commanderId,
                                     home['x'],
                                     home['y'],
                                     home['z'])

            if (jd.get('topic') == EventTopic.EVENT_UNIT_FINISHED
                and jd['data']['unit'] == commanderId):

                commanderPos = jd['meta']['atPos']
                build_at = commanderPos
                commands.build_at(self.request,
                                  unitId,
                                  "armlab",
                                  build_at['x'],
                                  build_at['y'],
                                  build_at['z'])

            elif jd.get('topic') == EventTopic.EVENT_UNIT_IDLE \
                or jd.get('topic') == EventTopic.EVENT_UNIT_FINISHED:

                unitId = jd['data']['unit']
                # Detect when the commander is ready
                if unitId in builders:
                    unitPos = jd['meta']['atPos']

                    if metal < 0.5 and len(build_queue) > 0:
                        build_queue.sort(key=distance_to(unitPos),
                                         reverse=True)
                        build_at = build_queue.pop()
                        commands.build_at(self.request,
                                        unitId,
                                        "armmex",
                                        build_at['x'],
                                        build_at['y'],
                                        build_at['z'])
                    elif energy < 40:
                        build_at = unitPos
                        commands.build_at(self.request,
                                        unitId,
                                        "armsolar",
                                        build_at['x'],
                                        build_at['y'],
                                        build_at['z'])
                    else:
                        build_at = unitPos
                        commands.build_at(self.request,
                                        unitId,
                                        random.choice(stuff),
                                        build_at['x'],
                                        build_at['y'],
                                        build_at['z'])
                elif jd['meta']['def'] == "armlab":
                    commands.build_at(self.request,
                    unitId, random.choice(lab_build))

            if (jd.get('topic') == EventTopic.EVENT_UNIT_FINISHED
                and jd['meta']['def'] in army_units):

                army.add(unitId)
                pos = random.choice(spots)
                commands.move_to(self.request,
                                unitId,
                                pos['x'],
                                pos['y'],
                                pos['z'])

            elif (jd.get('topic') == EventTopic.EVENT_UNIT_FINISHED
                  and jd['meta']['def'] == "armck"):

                builders.add(unitId)


            if jd.get('topic') == EventTopic.EVENT_UNIT_DESTROYED:
                if jd['data']['unit'] in army:
                    army.remove(jd['data']['unit'])
                elif jd['data']['unit'] in builders:
                    builders.remove(jd['data']['unit'])

            if jd.get('topic') == EventTopic.EVENT_ENEMY_ENTER_LOS \
               or jd.get('topic') == EventTopic.EVENT_ENEMY_ENTER_RADAR:

                enemy_pos = jd['meta']['atPos']
                self.add_point(frame, points, enemy_pos['x'], enemy_pos['y'], enemy_pos['z'], "ENEMY DETECTED", 100)
                for soldier in army:
                   commands.move_to(self.request,
                                    soldier,
                                    enemy_pos['x'],
                                    enemy_pos['y'],
                                    enemy_pos['z'])

if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 8765

    socketserver.TCPServer.allow_reuse_address = True
    with socketserver.TCPServer((HOST, PORT), MyTCPHandler) as server:
        server.serve_forever()
