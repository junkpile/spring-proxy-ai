from enum import IntEnum
import json

class CommandTopic(IntEnum):
    '''
    These mappings are from the AISCommands.h in the spring engine.
    '''
    COMMAND_NULL                                  =  0
    COMMAND_DRAWER_POINT_ADD                      =  1
    COMMAND_DRAWER_LINE_ADD                       =  2
    COMMAND_DRAWER_POINT_REMOVE                   =  3
    COMMAND_SEND_START_POS                        =  4 # Not implemented
    COMMAND_CHEATS_SET_MY_INCOME_MULTIPLIER       =  5 # Not implemented
    COMMAND_SEND_TEXT_MESSAGE                     =  6
    COMMAND_SET_LAST_POS_MESSAGE                  =  7 # Not implemented
    COMMAND_SEND_RESOURCES                        =  8 # Not implemented
    COMMAND_SEND_UNITS                            =  9 # Not implemented
    COMMAND_UNUSED_0                              = 10 # unused
    COMMAND_UNUSED_1                              = 11 # unused
    COMMAND_GROUP_CREATE                          = 12 # unused
    COMMAND_GROUP_ERASE                           = 13 # unused
    COMMAND_GROUP_ADD_UNIT                        = 14 # unused
    COMMAND_GROUP_REMOVE_UNIT                     = 15 # unused
    COMMAND_PATH_INIT                             = 16 # Not implemented
    COMMAND_PATH_GET_APPROXIMATE_LENGTH           = 17 # Not implemented
    COMMAND_PATH_GET_NEXT_WAYPOINT                = 18 # Not implemented
    COMMAND_PATH_FREE                             = 19 # Not implemented
    COMMAND_CHEATS_GIVE_ME_RESOURCE               = 20 # Not implemented
    COMMAND_CALL_LUA_RULES                        = 21 # Not implemented
    COMMAND_DRAWER_ADD_NOTIFICATION               = 22 # Not implemented
    COMMAND_DRAWER_DRAW_UNIT                      = 23 # Not implemented
    COMMAND_DRAWER_PATH_START                     = 24 # Not implemented
    COMMAND_DRAWER_PATH_FINISH                    = 25 # Not implemented
    COMMAND_DRAWER_PATH_DRAW_LINE                 = 26 # Not implemented
    COMMAND_DRAWER_PATH_DRAW_LINE_AND_ICON        = 27 # Not implemented
    COMMAND_DRAWER_PATH_DRAW_ICON_AT_LAST_POS     = 28 # Not implemented
    COMMAND_DRAWER_PATH_BREAK                     = 29 # Not implemented
    COMMAND_DRAWER_PATH_RESTART                   = 30 # Not implemented
    COMMAND_DRAWER_FIGURE_CREATE_SPLINE           = 31 # Not implemented
    COMMAND_DRAWER_FIGURE_CREATE_LINE             = 32 # Not implemented
    COMMAND_DRAWER_FIGURE_SET_COLOR               = 33 # Not implemented
    COMMAND_DRAWER_FIGURE_DELETE                  = 34 # Not implemented
    COMMAND_UNIT_BUILD                            = 35
    COMMAND_UNIT_STOP                             = 36 # Not implemented
    COMMAND_UNIT_WAIT                             = 37 # Not implemented
    COMMAND_UNIT_WAIT_TIME                        = 38 # Not implemented
    COMMAND_UNIT_WAIT_DEATH                       = 39 # Not implemented
    COMMAND_UNIT_WAIT_SQUAD                       = 40 # Not implemented
    COMMAND_UNIT_WAIT_GATHER                      = 41 # Not implemented
    COMMAND_UNIT_MOVE                             = 42 # Not implemented
    COMMAND_UNIT_PATROL                           = 43 # Not implemented
    COMMAND_UNIT_FIGHT                            = 44 # Not implemented
    COMMAND_UNIT_ATTACK                           = 45 # Not implemented
    COMMAND_UNIT_ATTACK_AREA                      = 46 # Not implemented
    COMMAND_UNIT_GUARD                            = 47 # Not implemented
    COMMAND_UNIT_AI_SELECT                        = 48 # Not implemented
    COMMAND_UNIT_GROUP_ADD                        = 49 # Not implemented
    COMMAND_UNIT_GROUP_CLEAR                      = 50 # Not implemented
    COMMAND_UNIT_REPAIR                           = 51 # Not implemented
    COMMAND_UNIT_SET_FIRE_STATE                   = 52 # Not implemented
    COMMAND_UNIT_SET_MOVE_STATE                   = 53 # Not implemented
    COMMAND_UNIT_SET_BASE                         = 54 # Not implemented
    COMMAND_UNIT_SELF_DESTROY                     = 55 # Not implemented
    COMMAND_UNIT_SET_WANTED_MAX_SPEED             = 56 # Not implemented
    COMMAND_UNIT_LOAD_UNITS                       = 57 # Not implemented
    COMMAND_UNIT_LOAD_UNITS_AREA                  = 58 # Not implemented
    COMMAND_UNIT_LOAD_ONTO                        = 59 # Not implemented
    COMMAND_UNIT_UNLOAD_UNITS_AREA                = 60 # Not implemented
    COMMAND_UNIT_UNLOAD_UNIT                      = 61 # Not implemented
    COMMAND_UNIT_SET_ON_OFF                       = 62 # Not implemented
    COMMAND_UNIT_RECLAIM_UNIT                     = 63 # Not implemented
    COMMAND_UNIT_RECLAIM_AREA                     = 64 # Not implemented
    COMMAND_UNIT_CLOAK                            = 65 # Not implemented
    COMMAND_UNIT_STOCKPILE                        = 66 # Not implemented
    COMMAND_UNIT_D_GUN                            = 67 # Not implemented
    COMMAND_UNIT_D_GUN_POS                        = 68 # Not implemented
    COMMAND_UNIT_RESTORE_AREA                     = 69 # Not implemented
    COMMAND_UNIT_SET_REPEAT                       = 70 # Not implemented
    COMMAND_UNIT_SET_TRAJECTORY                   = 71 # Not implemented
    COMMAND_UNIT_RESURRECT                        = 72 # Not implemented
    COMMAND_UNIT_RESURRECT_AREA                   = 73 # Not implemented
    COMMAND_UNIT_CAPTURE                          = 74 # Not implemented
    COMMAND_UNIT_CAPTURE_AREA                     = 75 # Not implemented
    COMMAND_UNIT_SET_AUTO_REPAIR_LEVEL            = 76 # Not implemented
    COMMAND_UNIT_SET_IDLE_MODE                    = 77 # Not implemented
    COMMAND_UNIT_CUSTOM                           = 78 # Not implemented
    COMMAND_CHEATS_GIVE_ME_NEW_UNIT               = 79 # Not implemented
    COMMAND_TRACE_RAY                             = 80 # Not implemented
    COMMAND_PAUSE                                 = 81 # Not implemented
    COMMAND_UNIT_RECLAIM_FEATURE                  = 82 # Not implemented
    # COMMAND_UNIT_ATTACK_POS
    # COMMAND_UNIT_INSERT
    # COMMAND_UNIT_REMOVE
    # COMMAND_UNIT_ATTACK_AREA
    # COMMAND_UNIT_ATTACK_LOOPBACK
    # COMMAND_UNIT_GROUP_SELECT
    # COMMAND_UNIT_INTERNAL
    COMMAND_DEBUG_DRAWER_GRAPH_SET_POS            = 83 # Not implemented
    COMMAND_DEBUG_DRAWER_GRAPH_SET_SIZE           = 84 # Not implemented
    COMMAND_DEBUG_DRAWER_GRAPH_LINE_ADD_POINT     = 85 # Not implemented
    COMMAND_DEBUG_DRAWER_GRAPH_LINE_DELETE_POINTS = 86 # Not implemented
    COMMAND_DEBUG_DRAWER_GRAPH_LINE_SET_COLOR     = 87 # Not implemented
    COMMAND_DEBUG_DRAWER_GRAPH_LINE_SET_LABEL     = 88 # Not implemented
    COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_ADD       = 89 # Not implemented
    COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_UPDATE    = 90 # Not implemented
    COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_DELETE    = 91 # Not implemented
    COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_POS   = 92 # Not implemented
    COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_SIZE  = 93 # Not implemented
    COMMAND_DEBUG_DRAWER_OVERLAYTEXTURE_SET_LABEL = 94 # Not implemented
    COMMAND_TRACE_RAY_FEATURE                     = 95 # Not implemented
    COMMAND_CALL_LUA_UI                           = 96 # Not implemented

def say(request, message):
    data = {
            "data": {
                "text": message,
                "zone": -1
            },
            "topic": CommandTopic.COMMAND_SEND_TEXT_MESSAGE
        }
    command(request, data)

def add_point(request, x, y, z, label):
    data = {
            "data": {
                "label": label,
                "pos": {
                    "x": x,
                    "y": y,
                    "z": z
                }
            },
            "topic": CommandTopic.COMMAND_DRAWER_POINT_ADD
        }
    command(request, data)

def remove_point(request, x, y, z):
    data = {
            "data": {
                "pos": {
                    "x": x,
                    "y": y,
                    "z": z
                }
            },
            "topic": CommandTopic.COMMAND_DRAWER_POINT_REMOVE
        }
    command(request, data)

def add_line(request, x_from, y_from, z_from, x_to, y_to, z_to):
    data = {
            "data": {
                "posFrom": {
                    "x": x_from,
                    "y": y_from,
                    "z": z_from
                },
                "posTo": {
                    "x": x_to,
                    "y": y_to,
                    "z": z_to
                }
            },
            "topic": CommandTopic.COMMAND_DRAWER_LINE_ADD
        }
    command(request, data)

def move_to(request, unitId, x, y, z):
    data = {
            "data": {
                "unitId": unitId,
                "groupId": -1,
                "options": 0,
                "toPos": {
                    "x": x,
                    "y": y,
                    "z": z
                }
            },
            "topic": CommandTopic.COMMAND_UNIT_MOVE
        }
    command(request, data)

def build_at(request, unitId, toBuildUnitDef, x=0, y=0, z=0, timeout=None, minDistance=10, searchRadius=250):
    data = {
            "data": {
                "unitId": unitId,
                "groupId": -1,
                "options": 0,
                "toBuildUnitDef": toBuildUnitDef,
                "facing": 0,
                "buildPos": {
                    "x": x,
                    "y": y,
                    "z": z
                },
                "minDistance": minDistance,
                "searchRadius": searchRadius
            },
            "topic": CommandTopic.COMMAND_UNIT_BUILD
        }
    if timeout:
        data["data"]["timeout"] = timeout
    command(request, data)

def command(request, data):
    send = "{}\n".format(json.dumps(data))
    request.sendall(send.encode('utf-8'))
    print("sent: {}".format(data))
