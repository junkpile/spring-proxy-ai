#ifndef _PROXYAI_COMMAND_H
#define _PROXYAI_COMMAND_H

#include "json.hpp"

using nlohmann::json;

namespace proxyai {
    struct Command {
        int topic;
        json data;
    };
};

#endif
