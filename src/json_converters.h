#ifndef __JSON_CONVERTERS_H__
#define __JSON_CONVERTERS_H__
#include "ExternalAI/Interface/AISEvents.h"
#include "ExternalAI/Interface/AISCommands.h"
#include "json.hpp"
#include "Command.h"
#include "AIFloat3.h"

using nlohmann::json;

struct ProxySBuildUnitCommand {
    int unitId;
    int groupId;
    short options;
    std::string toBuildUnitDef;
    float* buildPos_posF3;
    int facing;
    int timeout;
    float searchRadius;
    int minDistance;
};

// Command deserializers
namespace proxyai {
    void from_json(const json& j, struct Command& c);
};

void from_json(const json& j, struct SSendTextMessageCommand& c);
void from_json(const json& j, struct SMoveUnitCommand& c);
void from_json(const json& j, struct ProxySBuildUnitCommand& c);
void from_json(const json &j, struct SAddPointDrawCommand &c);
void from_json(const json &j, struct SRemovePointDrawCommand &c);
void from_json(const json &j, struct SAddLineDrawCommand &c);

// Event serializers
void to_json(json& j, const SInitEvent& e);
void to_json(json& j, const SReleaseEvent& e);
void to_json(json& j, const SUpdateEvent& e);
void to_json(json& j, const SMessageEvent& e);
void to_json(json& j, const SLuaMessageEvent& e);
void to_json(json& j, const SUnitCreatedEvent& e);
void to_json(json& j, const SUnitFinishedEvent& e);
void to_json(json& j, const SUnitIdleEvent& e);
void to_json(json& j, const SUnitMoveFailedEvent& e);
void to_json(json& j, const SUnitDamagedEvent& e);
void to_json(json& j, const SUnitDestroyedEvent& e);
void to_json(json& j, const SUnitGivenEvent& e);
void to_json(json& j, const SUnitCapturedEvent& e);
void to_json(json& j, const SEnemyEnterLOSEvent& e);
void to_json(json& j, const SEnemyLeaveLOSEvent& e);
void to_json(json& j, const SEnemyEnterRadarEvent& e);
void to_json(json& j, const SEnemyLeaveRadarEvent& e);
void to_json(json& j, const SEnemyDamagedEvent& e);
void to_json(json& j, const SEnemyDestroyedEvent& e);
void to_json(json& j, const SWeaponFiredEvent& e);
void to_json(json& j, const SPlayerCommandEvent& e);
void to_json(json& j, const SCommandFinishedEvent& e);
void to_json(json& j, const SSeismicPingEvent& e);
void to_json(json& j, const SLoadEvent& e);
void to_json(json& j, const SSaveEvent& e);
void to_json(json& j, const SEnemyCreatedEvent& e);
void to_json(json& j, const SEnemyFinishedEvent& e);
void to_json(json &j, const springai::AIFloat3& f);
#endif
