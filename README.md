# spring-proxy-ai
A Spring Engine AI that acts as a proxy for AIs written any language.

## ProxyAI

An AI in Spring Engine needs to be compiled and linked with a matching version of the game engine. AI:s are normally part of the Spring codebase and compiled together with Spring.

The basic idea with ProxyAI is to make a small translation layer between Spring Engine and the AI logic, the communication happens over a TCP socket using JSON to make it easy to use with any language. This project bundles a simple example Python AI.

The AI communicates with the ProxyAI on a communication channel of topics. Messages sent *to* ProxyAI are called **commands**, and messages received *from* ProxyAI are called **events**.

```
               --- commands --->
   Client AI                         ProxyAI    <---->   Spring Engine
               <---  events  ---
```

## JSON format

The content of *meta* and *data* varies depending on *topic*.

We mostly use the same *topic* number as Spring Engine, all topics are not implemented. We have implemented a few events of our own, they have a topic number of 10000 or higher. The *meta* field was added by us to enrich the data with additional information not normally part of the event.

### Commands

```
{
    "data": <object>,
    "topic": <integer>
}
```

For a list of implemented topics see `exampleai/commands.py`.

### Events

```
{
    "data": <object>,
    "meta": <object>,
    "topic": <integer>
}
```

The event EVENT_UPDATE is emitted every 30th frame (normally every 500ms). This contains extra metadata like economy information. This event can also be used to synchronize yourself with the game speed.

For a list of topics see `exampleai/events.py`.
